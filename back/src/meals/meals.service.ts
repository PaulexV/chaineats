import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { meal, MealDocument } from './meals.schema';
import { CreateMealDto } from './create-meal.dto';
import { Document } from 'mongodb';

@Injectable()
export class MealsService {
  constructor(@InjectModel(meal.name) private mealModel: Model<MealDocument>) {}

  async getMeals(): Promise<meal[]> {
    return this.mealModel.find().exec();
  }

  async getMeal(mealId: string): Promise<meal> {
    return this.mealModel.findById(mealId).exec();
  }

  async createMeal(createMealDto: CreateMealDto): Promise<meal> {
    const createdMeal = new this.mealModel(createMealDto);
    return createdMeal.save();
  }
}
