import { Controller, Get, Param } from '@nestjs/common';
import { ValidateObjectId } from 'src/shared/validateObjectId.pipes';
import { meal } from './meals.schema';
import { MealsService } from './meals.service';
import { Document } from 'mongodb';

@Controller('meals')
export class MealsController {
  constructor(private readonly mealsService: MealsService) {}

  @Get()
  async getMeals(): Promise<meal[]> {
    return this.mealsService.getMeals();
  }

  @Get(':mealId')
  async getMeal(
    @Param('mealId', new ValidateObjectId()) mealId: string,
  ): Promise<meal> {
    return this.mealsService.getMeal(mealId);
  }
}
