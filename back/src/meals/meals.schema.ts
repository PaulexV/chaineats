import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

export type MealDocument = mongoose.Document & meal;

@Schema()
export class meal {
  @Prop()
  name: string;

  @Prop()
  type: string;

  @Prop()
  price: number;

  @Prop()
  picture: string;

  @Prop()
  ingredient: [
    {
      ingredientName: string;
      quantity: number;
      origin: {
        vendor: {
          id: number;
          name: string;
          location: {
            city: string;
            region: string;
          };
          transportation: {
            checkpoint1: string;
            checkpoint2: string;
            checkpoint3: string;
          };
        };
      };
    },
  ];
}

export const MealSchema = SchemaFactory.createForClass(meal);
