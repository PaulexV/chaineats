export class CreateMealDto {
  name: string;
  type: string;
  price: number;
  picture: string;
  ingredient: [
    {
      ingredientName: string;
      quantity: number;
      origin: {
        vendor: {
          id: number;
          name: string;
          location: {
            city: string;
            region: string;
          };
          transportation: {
            checkpoint1: string;
            checkpoint2: string;
            checkpoint3: string;
          };
        };
      };
    },
  ];
}
