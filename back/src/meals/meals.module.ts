import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MealsController } from './meals.controller';
import { MealsService } from './meals.service';
import { meal, MealDocument, MealSchema } from './meals.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: meal.name, schema: MealSchema }]),
  ],
  controllers: [MealsController],
  providers: [MealsService],
})
export class MealsModule {}
