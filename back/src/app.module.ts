import { MealsModule } from './meals/meals.module';
import { MongooseModule } from '@nestjs/mongoose';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [MongooseModule.forRoot('mongodb://127.0.0.1/admin'), MealsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
