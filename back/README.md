# ChainEats

## Projet de labo Initiation à la crypto

### Le principe

> Utiliser les supply chains afin de connaitre la provenance des plats commandés.
>
> Le but final est de pouvoir commander un plat, cliquer sur l'un des ingrédients et
>en voir toutes les informations au préalable stockées dans la chain.
>
> Ex.: Je commande un burger, je clique sur le steak, je vois la provenance, comment il a été acheminé etc...

### Les technos

> JS pour le back
> Mongodb pour la BDD
> A voir pour les frameworks à utiliser par la suite.
