plats:	- Burger
	- Salade composée
	- Flan pâtissier

Détail des plats:
	- Burger: 
		- Bun (pain x2)
		- Viande
		- Salade
		- Tomates
		- Sauce (tkt c secré)
		- Fromage

	- Salade composée:
		- Salade (Iceberg)
		- Tomates
		- Gésier
		- Croutons de pain
		- Oeufs durs
		- Sauce vinaigrette

	- Flan pâtissier:
		- Oeufs
		- Lait
		- Sucre
		- Vanille
		- Pâte brisée
		- Maïzena