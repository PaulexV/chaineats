import React from 'react';
import { Route, Routes } from 'react-router-dom';
import './App.css';
import Index from './Components/Index';
import Appbar from './Components/Mui/Appbar';
import Meal from './Components/Meal';

function App() {
  return (
    <div className="App">
      <Appbar />
      <header className="App-header">
        <Routes>
          <Route path="/" element={<Index />} />
          <Route path="/:meal" element={<Meal />} />
        </Routes>
      </header>
    </div>
  );
}

export default App;
