import { Card, CardContent, Typography, CardActions, Button, CardMedia } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

function Index() {

  // get meals from api at localhost:3000/meals
  const [meals, setMeals] = useState<any>([])

  useEffect(() => {
    fetch('http://localhost:3000/meals')
      .then(res => res.json())
      .then(data => setMeals(data))
  }, [])


  return (
    <div>
      <h1 className='title'>Meals</h1>
      <div className="meal-grid">
        {meals.map((meal: any): JSX.Element => (
          <Card key={meal.id} sx={{ maxWidth: 345 }}>
            <CardMedia
              component="img"
              height="140"
              image={meal.picture}
              alt="green iguana" />
            <CardContent>
              <Typography gutterBottom variant="h4" component="div">
                {meal.name}
              </Typography>
              <Typography variant="body1" component="div">
                Type: {meal.type}
              </Typography>
              <Typography variant="body1" component="div">
                Prix: {meal.price} €
              </Typography>
            </CardContent>
            <CardActions>
              <Link to={{ pathname: `/${meal.name}` }}>
                <Button size="small">Plus de détails</Button>
              </Link>
            </CardActions>
          </Card>
        ))}
      </div>
    </div>
  )
}

export default Index